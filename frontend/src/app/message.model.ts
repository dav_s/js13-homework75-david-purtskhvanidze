export class Message {
  constructor(
    public encoded: string,
    public decoded: string,
  ) {}
}
