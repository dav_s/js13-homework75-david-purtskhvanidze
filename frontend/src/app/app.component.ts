import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Message } from './message.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @ViewChild('f') form!: NgForm;

  message!: Message;

  constructor(private http: HttpClient) {}

  textEncode() {
    const body = {
      password: this.form.value.password,
      message: this.form.value.message
    };
    return this.http.post('http://localhost:8000/encode', body).subscribe(encode => {
      this.message = <Message>encode
    });
  }

  textDecode() {
    const body = {
      password: this.form.value.password,
      message: this.form.value.message
    };
    return this.http.post('http://localhost:8000/decode', body).subscribe(decode => {
      this.message = <Message>decode
    });
  }

}
