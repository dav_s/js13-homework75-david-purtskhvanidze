const express = require('express');
const vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');
const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());

app.post('/encode', (req, res) => {
    const encodeBoby = {
        password: req.body.password,
        message: req.body.message,
    }
    return res.send(
        {encoded: vigenere.Cipher(encodeBoby.password).crypt(encodeBoby.message)}
    );
});

app.post('/decode', (req, res) => {
    const decodeBoby = {
        password: req.body.password,
        message: req.body.message,
    }
    return res.send(
        {decoded: vigenere.Decipher(decodeBoby.password).crypt(decodeBoby.message)}
    );
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});